<?php
class Payload {
  private $title;
  private $url;
  private $description;
  private $systemaccess;
  private $loginaccess;
  private $OS;
  private $output;
  private $requirements;

  function __construct($p, $t = "", $u = "", $d = "", $s = "", $l = "", $o = "", $ou = "", $r = "") {
    if ($t !== "" && $p == null) {
      $this->title = $t;
      $this->url = $u;
      $this->description = $d;
      $this->systemaccess = $s;
      $this->loginaccess = $l;
      $this->OS = $o;
      $this->output = $ou;
      $this->requirements = $r;
    } else {
    $this->parse($p);
    }
  }

  private function parse($p){
    $array = explode("\n", $p);
    //title -> string
    $this->title = $array[0];

    //url -> string -> https://*
    if (substr($array[2],0,2) == "{%") {
      $u = explode('"',$array[2])[1];
      $this->url = $u;
    }
    $rest = explode("\n### ",$p);

    //description -> string
    $this->description = trim(explode("%}",$rest[0],2)[1]);

    $access = str_replace("Access level\n","",$rest[1]);
    $access = explode("* **Login**",$access);

    //system -> string
    $system = str_replace("* **System**","",$access[0]);
    $reg = preg_quote('[x]', '~'); // set regex
    $system = preg_grep('~' . $reg . '~', explode("*",$system)); //grep selected
    $system = implode("-",$system);
    $system = trim(str_replace("[x]","",$system));
    $this->systemaccess = $system;

    //login -> string
    $login = $access[1];
    $reg = preg_quote('[x]', '~'); // set regex
    $login = preg_grep('~' . $reg . '~', explode("*",$login)); //grep selected
    $login = implode("-",$login);
    $login = trim(str_replace("[x]","",$login));
    $this->loginaccess = explode(",",$login);

    //os -> string[]
    $os = str_replace("OS\n","",$rest[2]);
    $reg = preg_quote('[x]', '~'); // set regex
    $os = preg_grep('~' . $reg . '~', explode("*",$os)); //grep selected
    $os = implode("-",$os);
    $os = trim(str_replace("\n","",str_replace(" ","",str_replace("[x]","",$os))));
    $os = explode("-",$os);
    $this->OS = $os;

    //output -> string
    $output = str_replace("Output\n\n>","",$rest[3]);
    $this->output = $output;

    //requirements -> string
    $requirements = str_replace("Requirements\n","",$rest[4]);
    $this->requirements = $requirements;
  }

  public function getTitle(){
    return $this->title;
  }
  public function getUrl(){
    return $this->url;
  }
  public function getDescription(){
    return $this->description;
  }
  public function getSystemAccess(){
    return $this->systemaccess;
  }
  public function getLoginAccess(){
    return $this->loginaccess;
  }
  public function getOS(){
    return $this->OS;
  }
  public function getOutput(){
    return $this->outpt;
  }
  public function getRequirements(){
    return $this->requirements;
  }

  public function toString(){
    $output = "<h1>$this->title</h1><a href='$this->url'>View on github</a><p>$this->description</p><p>System access: $this->systemaccess</p><p>Login access: ".implode(", ",$this->loginaccess)."</p><p>OS: ".implode(", ",$this->OS)."</p><p>Output: $this->output</p><p>Requirements: $this->requirements</p>";
    return $output;
  }

}
?>
