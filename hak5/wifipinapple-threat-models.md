# WifiPinapple Threat Models

## King in the Middle

> This attack is like a Man in the Middle, but instead of just a single thing in the middle, we use and entire network: DNS server, DHCP server, Router ...

{% hint style="info" %}
More info here:
{% endhint %}

{% embed url="https://blog.haywirehax.com/writeups/2019/king-in-the-middle-attack" %}

## Rogue AP

### Summary

> Here the Wifi Pinapple will be used to spoof a network so we can actively exploit and phish the target.

### Steps

* First we setup the connection from the pinapple to the outside network, this is to avoid suspicion from the target.
  * If there's an ethernet cable, use that one, if not, use a mobile hotspot or crack the wifi.
* Then we need to check which wifi SSID's the victim is connected to/has at least once connected to.
* Then we spoof that SSID so the victim automatically connects without suspicion.
* At last, we can proceed to phish the target or we can use a proxy to see some usefull trafic or serve payloads.

### Check victims SSID's



