## Dns-spoof

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/dns-spoof" %}

> dnsspoof forges replies to arbitrary DNS address / pointer queries on the LAN. This is useful in bypassing hostname-based access controls, or in implementing a variety of man-in-the-middle attacks.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script
