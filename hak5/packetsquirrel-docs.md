# PacketSquirrel Docs

## Usage

### Use case

> Plugin the PacketSquirrel usb with the payloads loaded, then connect the micro usb to a power source and bridge the ethernet cable to create a man-in-the-middle

### Details

![](../.gitbook/assets/packet-squirrel-7b.jpg)

#### Payload 1 Default

> tcpdump: plugin the usb, plug in the ethernet cables, set switch to position: payload 1
>
> This will start a tcpdump that dumps all traffic from the PacketSquirrel to the victim.

#### Payload 2 default

> dnsspoof: setup the PacketSquirrel by configuring the switch2/spoofhost config file. You can use something like `address=/#/DNSServer` to make every request redirect to your server, or something like `address=/facebook.com/8.8.8.8` to redirect a single entry

#### Payload 3 VPN

> copy the `config.ovpn` file to the `switch3/` folder and boot the PacketSquirrem into payload 3

### Setup

* Plug the PacketSquirrel into a power source via the `micro usb`
* Connect it to your pc with an ethernet cable to the `Ethernet in` port
* you can ssh to it via `ssh 172.16.32.1` 

## Payloads

{% embed url="https://github.com/hak5/packetsquirrel-payloads" %}

> The payloads Are stored on the Packet Squirrel usb drive, to update just run `git pull`

More  about payload development on page 16-24 in the manual

