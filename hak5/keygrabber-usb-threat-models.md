# Keylogger payloads

## USB Keylogger

{% embed url="https://www.keelog.com/files/KeyGrabberUsbUsersGuide.pdf" %}

> Use the Keylogger USB in the HAK5 packet to steal keystrokes from a victim! This is easily done by plugging the device in between the victim and the keyboard.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Connect a keyboard to the keylogger and press `K+B+S`. This will change the keylogger to a storage device and the logs will be in `LOG.txt`

### Requirements

* Setup is pretty simple, check the config options on page 6 of the manual if needed, but will work out of the box.
