# Lan Turtle modules

## QuickCreds

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/QuickCreds" %}

> Snagging creds from locked machines --Mubix, Room362.com. Implements responder attack and saves creds to numbered directories in /root/loot. LED will blink rapidly while QuickCreds is running. Upon capture of NTLM hash the amber LED will light solid. Author: Hak5Darren. Credit: Mubix.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Output will be put in the `/root/loot/` folder

### Requirements

* Check the script

## Clomac

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/clomac" %}

> Clone Client's MAC address into WAN interface

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script

## Cron

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/cron" %}

> Schedule Tasks via cron

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script

## Ddnsc

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/ddnsc" %}

> DDNSC - Dynamic Domain Name Service Client

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script

## Dns-spoof

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/dns-spoof" %}

> dnsspoof forges replies to arbitrary DNS address / pointer queries on the LAN. This is useful in bypassing hostname-based access controls, or in implementing a variety of man-in-the-middle attacks.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script

## Dnsmasq-spoof

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/dnsmasq-spoof" %}

> DNSSpoof using DNSMasq instead of Dsniff tools

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> None

### Requirements

* Check the script

## Follow-file

{% embed url="https://github.com/hak5/lanturtle-modules/blob/gh-pages/modules/follow-file" %}

> Follow log printing data as file grows

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> logs in loot

### Requirements

* Check the script
