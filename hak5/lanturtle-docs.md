# LanTurtle Docs

## Usage

### Use case

Can be used to gain remote access to a host via vpn, meterpreter, reverse ssh ...

More use cases on page 5-7 of the manual.

### Details

![](../.gitbook/assets/img_20190318_111900_edit.jpg)

### Setup

{% embed url="https://github.com/hak5/lanturtle-modules" %}

* Get the payloads from the link above
* SSH into the LanTurtle: `ssh 172.16.84.1`
* Copy the modules to `/etc/turtle/modules`
* run the command `turtle`
* edit the module configuration
* disconnect from the turtle
* ready to go!

