# WiFiPinapple Docs

## Usage

### Use case

> Can be used as a rogue access point for a man in the middle, enabling information and credential gathering and other forms of OSINT

### Details

![](../.gitbook/assets/brf0dzu.jpg)

For more details please refer to page 35-38 in the manual.

### Setup

> The web interface is accessible here:

{% embed url="http://172.16.42.1:1471" %}

{% hint style="warning" %}
All passwords are the default nviso root password
{% endhint %}

{% hint style="warning" %}
To connect to the management wifi you need to manually enter the ssid: `NVisoPineapple` because it is hidden.
{% endhint %}

> You can change the wifi name here:

{% embed url="http://172.16.42.1:1471/\#!/modules/Networking" %}

![](../.gitbook/assets/screen-shot-2019-03-05-at-13.55.28.png)

#### Dashboard

![](../.gitbook/assets/screen-shot-2019-03-05-at-10.39.16.png)

## Modules

> All modules are installed, including all dependencies

## Workflow

If you want a sample workflow, there is one available at page 11-16 in the manual.

## How does WiFi work?

For a detailed explanation on how wifi works please refer to page 17-30 in the manual.

