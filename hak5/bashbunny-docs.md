# BashBunny Docs

## Usage

### Use case

> Used for hot-plug attacks that need to be more advanced than a RubberDucky. The BashBunny is eventually Linux-on-a-stick. It can act as an Ethernet adapter, keyboard, mass-storage... literally any USB device.

### Details

![Switch positions](../.gitbook/assets/bb_diagram1.png)

#### Directory

* Docs: Documentation lies here
* Languages: HID keyboard layouts
* Loot: Payloads store logs and data here
* Tools: Used to install deb packages and tools
* Payloads: active payloads, library and extensions
  * switch1: payload for slider position farthest from the USB
  * switch2: payload for middle slider position
  * library: payloads library from BashBunny Payload git
    * extensions: script extensions
* config.txt: is sourced before any payload is executed, this allows for global configurations such as setting the keystroke injection language with the DUCKY\_LANG command.

#### Arming mode

> This enables Serial connection and Mass Storage. This mode is used for setup.

### Setup

The default credentials are root:hak5bunny, ssh is enabled but serial is recommended. The default ip is `172.16.64.1`.

Any tools placed in the tools will be installed on startup. Any deb file will be installed using dpkg.

Connect using serial:

1. `dmesg | grep tty` OR `ls /dev/tty*`
2. Then connect with screen: `screen /dev/tty`

## Payloads

> [https://github.com/hak5/bashbunny-payloads](https://github.com/hak5/bashbunny-payloads)
>
> The git above was placed on the BashBunny, to update please download the git, then copy the scripts to the BashBunny.
>
> More info on payload development on page 41-47 in the manual.

### Quack

Using ATTACKERMODE HID & QUACK switch1/text.txt you can inject the keystrokes in that file

## Extensions

Extensions are found in the aptly named folder: Extensions. They are commands that can be invoked by using a singly word in Ducky Script.

## More Documentation

> [https://wiki.bashbunny.com/\#!payload\_development.md](https://wiki.bashbunny.com/#!payload_development.md)

