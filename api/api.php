<?php
require_once 'Payload.php';
class API {
  private static $instance = null;
  private $bashbunnypayloads;
  private $AllPayloads;
  private $filter;

  function __construct() {
    $bashbunny = file_get_contents("../hak5/bashbunny-threat-models.md");
    $keygrabber = file_get_contents("../hak5/keygrabber-usb-threat-models.md");
    $mitreEAttack = json_decode(file_get_contents("mitre.json"), true);
    $bashbunnypayloads = explode("\n## ",$bashbunny);
    $keyloggerpayloads = explode("\n## ",$keygrabber);
    $this->AllPayloads = array_merge($bashbunnypayloads, $keyloggerpayloads);
    foreach ($this->AllPayloads as $key => $value) {
      $this->AllPayloads[$key] = new Payload($value);
    }
    foreach ($mitreEAttack["objects"] as $key => $t) {
      // foreach ($t as $title => $value) {
      //   echo "<h1>$title</h1><p>".var_dump($value)."</p>";
      // }
      array_push($this->AllPayloads,new Payload(null, $t["name"], $t["external_references"], $t["description"], "Not working yet", $t["x_mitre_permissions_required"], $t["x_mitre_platforms"], "", ""));
    }
  }

  //Singleton
  public static function getInstance()
  {
    if (self::$instance == null)
    {
      self::$instance = new API();
    }

    return self::$instance;
  }

  public function filter($os = [], $systemaccess = [], $loginaccess = []){
    $filter = $this->AllPayloads;
    foreach ($filter as $key => $payload) {
      if ($os !== []) {
        if (array_diff($os, $payload->getOS()) == $os) {
          unset($filter[$key]);
        }
      }
      if ($loginaccess !== []) {
        if (array_diff($loginaccess, $payload->getLoginAccess()) == $loginaccess) {
          unset($filter[$key]);
        }
      }
      if ($systemaccess !== []) {
        if (array_diff($systemaccess, explode(",",$payload->getSystemAccess())) == $systemaccess) {
          unset($filter[$key]);
        }
      }
    }
    return $this->filter = $filter;
  }

  public function toStringFilter(){
    $html = "";
    foreach ($this->filter as $key => $payload) {
      $html.= $payload->toString();
    }
    return $html;
  }

  public function getBashbunnyPayloads(){
    return $this->bashbunnypayloads;
  }

  public function getFilter(){
    return $this->filter;
  }

}
?>
